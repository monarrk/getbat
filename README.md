# getbat
painfully simple NetBSD utility to grab battery percentage from `sysenv.h`.

I am aware of tools such as `envstat(8)` for NetBSD, but those show a lot of information. I wanted to be able to get *just* the battery percentage for shell scripts.
